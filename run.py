# -*-coding:utf-8-*-

"""
  本模块提供接口路由
  分发处理视图
"""
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
from tornado.options import define, options
from impl.user import MainHandler, RefreshHandler, MessageRefreshHandler, PassivityRefreshHandler, \
    MessagePassivityRefreshHandler

define("port", default=8012, help="run on the given port", type=int)


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r'/', MainHandler),
            ('/refresh.html', RefreshHandler),
            ('/message/refresh', MessageRefreshHandler),
            ('/passivityRefresh.html', PassivityRefreshHandler),
            ('/message/passivityRefresh', MessagePassivityRefreshHandler),
        ]

        settings = dict(
            cookie_secret="some_long_secret_and_other_settins",
            template_path='webPage/template',  # 路径配置，就是自动取到该路径下寻找文件
            static_path='webPage/static',  # 静态文件配置,需要特殊处理
        )
        tornado.web.Application.__init__(self, handlers, **settings)


if __name__ == "__main__":
    tornado.options.parse_command_line()
    app = Application()
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    print('tornado 已启动! 端口为', options.port)
    tornado.ioloop.IOLoop.instance().start()
