import random


class Constant(object):
    """
        返回信息常量类
    """
    # OK
    OK = {'code': 200, 'info': 'OK'}

    # ERROR 错误信息
    # 您已在其他地方登录
    USERYETENTER = {'code': -1, 'info': '您已在其他地方登录'}
    # 执行错误
    EXECUTE_ERROR = {'code': -2, 'info': '执行错误'}
    # 该用户没有执行权限 请检查
    USERISNOTPM = {'code': -3, 'info': '该用户没有执行权限 请检查'}
    # 手机号不存在
    USERNAMENOT_EXIST = {'code': -4, 'info': '该用户不存在'}
    # 用户名或密码错误
    USERINEXISTENCE = {'code': -6, 'info': '用户名或密码错误'}
    # 手机号已存在
    USEREXIST = {'code': -7, 'info': '用户已存在'}
    # 验证码错误
    VERIFICATION_CODE_ERROR = {'code': -8, 'info': '验证码获取失败'}
    # 验证码超时
    VERIFICATION_CODE_TIME_OUT = {'code': -9, 'info': '验证码已超时'}
    # 要修改的内容不存在
    CONTENT_DOES_NOT_EXIST = {'code': -10, 'info': '内容不存在'}
    # 要修改的内容不存在
    CONTENT_DOES_NOT_EXIST_NO_UPDATE = {'code': -11, 'info': '要修改的内容不存在'}

    # 善意提醒
    # 请求参数非法
    ILLEGAL_ARGUMENTS = {'code': 0, 'info': '请求参数非法'}
    # 重复添加
    REPETITION_EXECUTE = {'code': 1, 'info': '重复添加'}
    # 该账号不能被添加
    EXECUTE_ADD = {'code': 2, 'info': '该账号不能被添加'}
    # 用户登录已过期
    LOGIN_OVERDUE = {'code': 3, 'info': '登录已过期,请重新登录'}
    # 注销成功
    LOGOUT = {'code': 200, 'info': '注销成功'}
    # 请求频繁
    REQUEST_FREQUENTLY = {'code': 202, 'info': '请求频繁'}


def to_data(data):
    hs = {'code': 200, 'info': 'OK', 'data': data}
    return hs


def to_param_error_data(data):
    hs = {'code': 500, 'info': data}
    return hs


def to_page_data(this_page, page_count, counts, data):
    """

    :param this_page: 此前第几页
    :type this_page:int
    :param page_count: 共第几页
    :type page_count:int
    :param counts: 总条数
    :type counts:int
    :param data: 数据列表
    :type data:list
    :return:
    """
    result = {'code': 200, 'info': 'OK', 'this_page': this_page, 'page_count': page_count, 'counts': counts,
              'data': data}
    return result
