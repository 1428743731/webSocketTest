# webSocketTest

#### 介绍
html webSocket

#### 软件架构
软件架构说明
html5 tornado

#### 使用说明

1. python run.py
2. 浏览器打开 http://127.0.0.1:8012/refresh.html
    即主动刷新页面
3. 浏览器打开 http://127.0.0.1:8012/passivityRefresh.html
    即被动刷新页面     
  不建议使用while True  会导致torando服务卡死 
    **建议使用 前端定时器发送请求 来实现实时刷新**
#### 参与贡献
感谢 https://www.cnblogs.com/wzjbg/p/6528593.html